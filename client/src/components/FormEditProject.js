import React from 'react'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import {Checkbox } from 'react-bootstrap'


const FormEditProject = ({listAllMember, listMember ,nameproject, closeFormEditProject,
    submitFormEditProject, isShowEditProject, handleChangeCheckBoxEdit}) => {
    const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={() => closeFormEditProject()}
        />,
        <FlatButton
          label="Submit"
          primary={true}
          keyboardFocused={true}
          onClick={() => submitFormEditProject()}
        />,
      ];
      return(
        <div>
            <Dialog
            title="Edit Project"
            actions={actions}
            modal={false}
            open={isShowEditProject}
            onRequestClose={() => closeFormEditProject()}
            >
                <div>
                    <span>Name</span>
                    <br/>
                    <TextField
                        id="1"
                        readOnly={true}
                        defaultValue={nameproject}
                        fullWidth={true}
                        
                    />
                    <br/>
                    <span>Member</span>
                    <br/>
                     {
                        listAllMember.map((element, index) => {
                            if(listMember.indexOf(element.name) !== -1)
                            {
                                return;
                            }
                            
                            return (
                                <Checkbox 
                                onChange={(e, value) => handleChangeCheckBoxEdit(e.target.checked, element.name)} 
                                style={{ marginLeft: '15px' }} key={element.name}>{element.name}</Checkbox>
                            )
                        })
                    }
                </div>
            </Dialog>
        </div>
      )
}

FormEditProject.propTypes = {

}

export default FormEditProject