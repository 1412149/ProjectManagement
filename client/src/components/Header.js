import React from 'react'
import {Toolbar, ToolbarGroup} from 'material-ui/Toolbar'
import FlatButton from 'material-ui/FlatButton'
const Header = ({history}) => (
    <Toolbar style={{backgroundColor: '#1A237E', height: 100,}}>
        <ToolbarGroup firstChild={true}>
            <FlatButton style={{color: '#FAFAFA', fontSize: 30, marginLeft: 100, fontFamily:'Sans-serif'}}> 
                <span >Project Management</span>
            </FlatButton>
            
         
        </ToolbarGroup>

    
        
    </Toolbar>
)
export default Header

