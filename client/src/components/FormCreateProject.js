import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import {Checkbox } from 'react-bootstrap'
const FormCreateProject = ({listAllMember, setNameProject, closeFormProject,
    submitFormProject, isShowProject, handleChangeCheckBox, isValidNameProject}) => {
    const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={() => closeFormProject()}
        />,
        <FlatButton
          label="Submit"
          primary={true}
          keyboardFocused={true}
          onClick={() => submitFormProject()}
        />,
      ];
      return(
        <div>
            <Dialog
            title="Create Project"
            actions={actions}
            modal={false}
            open={isShowProject}
            onRequestClose={() => closeFormProject()}
            >
                <div>
                    <span>Name</span>
                    <br/>
                    <TextField
                        id="1"
                        onChange={(e) => setNameProject(e.target.value)}
                        fullWidth={true}
                        errorText={isValidNameProject ? '' : 'INVALID'}
                    />
                    <br/>
                    <span>Member</span>
                    <br/>
                     {
                        listAllMember.map((element, index) => {
                            return (
                                <Checkbox  onChange={(e, value) => handleChangeCheckBox(e.target.checked, element.name)} style={{ marginLeft: '15px' }}  key={element.name}>{element.name}</Checkbox>
                            )
                        })
                    }
                </div>
            </Dialog>
        </div>
      )
}

FormCreateProject.propTypes = {
    
    closeFormProjectFrom: PropTypes.func,
    isShowProject: PropTypes.bool,
    submitFormProject: PropTypes.func
}

export default FormCreateProject