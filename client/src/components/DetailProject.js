import React from 'react';
import { Pager} from 'react-bootstrap'
const DetailProject = ({listProject, pageCur, totalPage, handleKeyPress, onclickPrev, onclickNext, showFormEditProject}) =>(
    <div>
        <div style ={{float: "right", paddingBottom:5, paddingRight: 5}} className='Aligner'> 
            <input style={{type: 'text', width: 30,height: 30, fontSize: 20}}
                    onKeyPress={(event) => {
                        if (event.key === "Enter"){
                            handleKeyPress(event.target)
                        }
                    }}
                    defaultValue={pageCur}     
            />
            <span style={{ fontSize: 20 }}>/{totalPage}</span>
            
        </div>
        
        
        <div>
            <table className="table table-responsive table-hover" >
                <thead>
                    <tr>
                        <th style={{width:"10%"}}>No</th>
                        <th style={{width:"20%"}}>Name project</th>
                        <th style={{width:"60%"}}>List member</th>
                        <th style={{width:"10%"}}>Action</th>
                    </tr>
                </thead>
                <tbody>
                {
                    listProject.map((element, index) => {
                        let listMember = "";
                        if(element.listMember.length > 0){
                            listMember = element.listMember[0];
                        }
                        for(let i= 1; i<element.listMember.length; i++){
                            listMember += ", " + element.listMember[i] ;
                        }
                        return (
                            <tr key={element.name}> 
                                <td>{index + 1 + 5*(pageCur-1)}</td>
                                <td>{element.name}</td>
                                <td>{listMember}</td>
                                <td>
                                    {
                                        <button onClick={() => showFormEditProject(element.name)} className="btn btn-danger" >Add member</button>
                                    }
                                </td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table >

            <Pager>
                <Pager.Item onClick={() =>{onclickPrev(pageCur)}} href="#" disabled={pageCur === 1}>&larr; Previous</Pager.Item>{'  '}
                <Pager.Item href="#" >{pageCur}</Pager.Item>{'  '}
                <Pager.Item onClick={() => onclickNext(pageCur)} href="#" disabled={pageCur === totalPage}>Next &rarr;</Pager.Item>
            </Pager>
        </div>
        
    </div>
)

    

DetailProject.propTypes = {
}


export default DetailProject