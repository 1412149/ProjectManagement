import React from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import {DropDownMenu, MenuItem} from 'material-ui';
const FormCreateMember = ({listAllProject, setName, setNumberPhone, closeForm,
    submitForm, isShow, isValidNameMember,
    isSelectDropProject,handleSelectDropProject,
    }) => {
    const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={() => closeForm()}
        />,
        <FlatButton
          label="Submit"
          primary={true}
          keyboardFocused={true}
          onClick={() => submitForm()}
        />,
      ];
      return(
        <div>
            <Dialog
            title="Create Memner"
            actions={actions}
            modal={false}
            open={isShow}
            onRequestClose={() => closeForm()}
            >
                <div>
                    <span>Name</span>
                    <br/>
                    <TextField
                        id="1"
                        onChange={(e) => setName(e.target.value)}
                        fullWidth={true}
                        errorText={isValidNameMember ? '' : 'INVALID'}
                    />
                    <br/>
                    <br/>
                    <span>Number Phone</span>
                    <br/>
                    <TextField
                        id="2"
                        onChange={(e) => setNumberPhone(e.target.value)}
                        fullWidth={true}
                    />
                    <br/>
                    <br/>
                    <span>Project</span>
                    <br/>
                    <DropDownMenu value={isSelectDropProject} onChange={(event, index, value) => { handleSelectDropProject(value) }}>
                        {
                            listAllProject.map((element, index) => {
                                return (
                                    <MenuItem value={element.name} primaryText={element.name} key={element.name} />
                                )
                            })
                        }
                    </DropDownMenu>
             
                </div>
            </Dialog>
        </div>
      )
}

FormCreateMember.propTypes = {
    setName: PropTypes.func,
    setNumberPhone: PropTypes.func,
    closeFrom: PropTypes.func,
    isShow: PropTypes.bool,
    submitForm: PropTypes.func
}

export default FormCreateMember