import React from 'react';

import Header from './Header'
import FormCreateMember from './FormCreateMember'

import FormCreateProject from './FormCreateProject'
import DetailProject from './DetailProject'
import FormEditProject from './FormEditProject'

import {Button } from 'react-bootstrap'

const Profile = ({listAllProject, listAllMember, setIsShowFromCreateMember, setIsShowFromCreateProject, 
    setName, setNumberPhone, closeForm,submitForm, isShow, isValidNameMember,
    setNameProject, closeFormProject, isShowProject,submitFormProject,isValidNameProject,
    isSelectDropProject, handleSelectDropProject, handleChangeCheckBox,
    listProject, handleKeyPress, onclickPrev, onclickNext, totalPage, pageCur, showFormEditProject,
    closeFormEditProject, isShowEditProject, submitFormEditProject, handleChangeCheckBoxEdit,nameproject, listMember
    }) => (

        <div>
            <div>
                <Header 
                />
            </div>
            <div>

                <div className="row" style={{ padding: 20, backgroundColor: '#FFFFFF' }}>
                    <div className="col-sm-12" >
                        
                        <div style={{ marginTop: 20 }}>
                            <Button
                                style={{ width: 300, marginRight: 10 }}
                                onClick={() => setIsShowFromCreateMember(true)}
                            >CREATE MEMBER
                            </Button>

                            <Button
                                style={{ width: 300, marginRight: 10 }}
                                onClick={() => setIsShowFromCreateProject(true)}
                            >CREATE PROJECT
                            </Button>
                            
                        </div>

                        <FormCreateMember
                            setName={(value) => setName(value)}
                            setNumberPhone={(value) => setNumberPhone(value)}
                            closeForm={() => closeForm()}
                            isShow={isShow}
                            submitForm={() => submitForm()}
                            listAllProject={listAllProject}
                            isSelectDropProject = {isSelectDropProject}
                            handleSelectDropProject={(value) => handleSelectDropProject(value)}
                            isValidNameMember={isValidNameMember}
                        />
                        <FormCreateProject
                            isValidNameProject={isValidNameProject}
                            setNameProject={(value) => setNameProject(value)}
                            closeFormProject={() => closeFormProject()}
                            isShowProject={isShowProject}
                            submitFormProject={() => submitFormProject()}
                            listAllMember={listAllMember}
                            handleChangeCheckBox={(e, value) => handleChangeCheckBox(e, value)}
                        />
                        <FormEditProject
                            nameproject={nameproject}
                            closeFormEditProject={() => closeFormEditProject()}
                            isShowEditProject={isShowEditProject}
                            submitFormEditProject={() => submitFormEditProject()}
                            listAllMember={listAllMember}
                            listMember = {listMember}
                            handleChangeCheckBoxEdit={(e, value) => handleChangeCheckBoxEdit(e, value)}
                        />
                        <DetailProject
                            listProject={listProject}
                            handleKeyPress={(event) => handleKeyPress(event)}
                            onclickPrev={(value) => { onclickPrev(value) }}
                            onclickNext={(value) => onclickNext(value)}
                            totalPage={totalPage}
                            pageCur={pageCur}
                            showFormEditProject ={(value) => { showFormEditProject(value) }}
                        />

                    </div>
                </div>
            </div>
        </div>
        
        
    )

Profile.propTypes = {


}


export default Profile