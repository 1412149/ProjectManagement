import React from  'react'
import Home from '../components/Home'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as HomeAction from '../actions/HomeAction'
class HomeContainer extends React.Component{


    componentDidMount(){
        this.props.actions.LoadMember();
        this.props.actions.LoadProject();
        this.props.actions.LoadDetailProject();
    }
    render(){
        return(
            <div>
                <Home
                    setIsShowFromCreateMember={(value) => this.props.actions.setIsShowFromCreateMember(value)}
                    setName={(value) => this.props.actions.setName(value)}
                    setNumberPhone={(value) => this.props.actions.setNumberPhone(value)}
                    closeForm={() => this.props.actions.closeForm()}
                    submitForm={() => this.props.actions.submitForm()}
                    isValidNameMember={this.props.isValidNameMember}
                    isValidNameProject={this.props.isValidNameProject}

                    setIsShowFromCreateProject={(value) => this.props.actions.setIsShowFromCreateProject(value)}
                    setNameProject={(value) => this.props.actions.setNameProject(value)}
                    closeFormProject={() => this.props.actions.closeFormProject()}
                    submitFormProject={() => this.props.actions.submitFormProject()}

                    isShow={this.props.isShow}
                    listAllMember={this.props.listAllMember}
                    isShowProject={this.props.isShowProject}
                    listAllProject={this.props.listAllProject}

                    isSelectDropProject = {this.props.isSelectDropProject}
                    handleSelectDropProject={(value) => this.props.actions.handleSelectDropProject(value)}

                    handleChangeCheckBox={(e, value) => this.props.actions.handleChangeCheckBox(e, value)}


                    listProject={this.props.listProject}
                    handleKeyPress={(event) => this.props.actions.handleKeyPress(event)}
                    pageCur ={this.props.pageCur}
                    totalPage ={this.props.totalPage}
                    onclickPrev = {(value) => {this.props.actions.onclickPrev(value)}}
                    onclickNext = {(value) => this.props.actions.onclickNext(value)}
                    showFormEditProject={(value) => this.props.actions.showFormEditProject(value)}

                    nameproject={this.props.nameproject}
                    closeFormEditProject={() => this.props.actions.closeFormEditProject()}
                    isShowEditProject={this.props.isShowEditProject}
                    submitFormEditProject={() => this.props.actions.submitFormEditProject()}
                    handleChangeCheckBoxEdit={(e, value) => this.props.actions.handleChangeCheckBoxEdit(e, value)}
                    listMember={this.props.listMember}
                />
            </div>
        )
        
    }
}
const mapStateToProps = state => ({
    isShow: state.HomeReducer.formMember.isShow,
    isShowProject: state.HomeReducer.formProject.isShowProject,
    listAllProject: state.HomeReducer.allState.listAllProject,
    isSelectDropProject: state.HomeReducer.inputs.isSelectDropProject,
    listAllMember: state.HomeReducer.allState.listAllMember,
    isValidNameMember: state.HomeReducer.formMember.isValidNameMember,
    isValidNameProject:state.HomeReducer.formProject.isValidNameProject,

    pageCur: state.HomeReducer.detailProject.pageCur,
    totalPage: state.HomeReducer.detailProject.totalPage,
    listProject: state.HomeReducer.detailProject.listProject,

    isShowEditProject: state.HomeReducer.editProject.isShowProject,
    nameproject:state.HomeReducer.editProject.nameProject,
    listMember: state.HomeReducer.editProject.listMember,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(HomeAction, dispatch)
})

HomeContainer.propTypes = {
    isShow: PropTypes.bool,

}

export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(HomeContainer)