import {CreateMember, CreateProject, inputsTypes,EditProject,detailTypes} from '../constants/ActionTypes' 
import axios from 'axios'

export const setName = value => ({
    type: CreateMember.SET_NAME,
    value
})

export const setNumberPhone = value => ({
    type: CreateMember.SET_NUMBER_PHONE,
    value
})
export const setIsShowFromCreateMember = value => ({
    type: CreateMember.SET_ISSHOW,
    value
})

export const closeForm = () => (dispatch) => {
    dispatch(setIsShowForm(false));
    dispatch(resetForm());
    dispatch(ResetInput());
}

export const setIsShowForm = value => ({
    type: CreateMember.SET_ISSHOW_FORM,
    value
})
export const resetForm = () => ({
    type: CreateMember.RESET_FORM
})

export const setIsValidNameMember = (value) => ({
    type: CreateMember.SET_IS_VALID_NAME_MEMBER,
    value
})

export const submitForm = () => async (dispatch, getState) => {
    let name = getState().HomeReducer.formMember.name;
    let numberPhone = Number(getState().HomeReducer.formMember.numberPhone);
    let project = getState().HomeReducer.inputs.isSelectDropProject;
    if(project === null)
    {
        project = "";
    }
    await axios.post('http://localhost:3030/createmember',{
        name: name,
        numberPhone: numberPhone,
        project: project
    }).then((Response) =>{
        if(Response.data.status === 'false'){
            dispatch(setIsValidNameMember(false));
        }
        else{
            dispatch(closeForm());
            dispatch(LoadMember());
            dispatch(LoadProject());
            dispatch(setPageCurrent(1)); 
            dispatch(LoadDetailProject(1));
        }
    });
   
}


// project
export const setNameProject = value => ({
    type: CreateProject.SET_NAME_PROJECT,
    value
})

export const setIsShowFromCreateProject = value => ({
    type: CreateProject.SET_ISSHOW_FORM_PROJECT,
    value
})

export const closeFormProject = () => (dispatch) => {
    dispatch(setIsShowFormProject(false));
    dispatch(resetFormProject());
    dispatch(resetListMemberInProject());
}

export const setIsShowFormProject = value => ({
    type: CreateProject.SET_ISSHOW_PROJECT,
    value
})
export const resetFormProject = () => ({
    type: CreateProject.RESET_FORM_PROJECT
})
export const resetListMemberInProject = () => ({
    type: CreateProject.RESET_LIST_MEMBER_IN_PROJECT
})
export const setIsValidNameProject = (value) => ({
    type: CreateProject.SET_IS_VALID_NAME_PROJECT,
    value
})

export const submitFormProject = () => async (dispatch, getState) => {
    let name = getState().HomeReducer.formProject.nameProject;
    let listMember = getState().HomeReducer.formProject.listMember;
    await axios.post('http://localhost:3030/createproject',{
        name: name,
        listMember: listMember
    }).then((Response) =>{
        if(Response.data.status === 'false'){
            dispatch(setIsValidNameProject(false));
        }else{
            dispatch(closeFormProject());
            dispatch(LoadProject());
            dispatch(setPageCurrent(1));  
            dispatch(LoadDetailProject(1));
        }
    });
   
}

////load data
export const setListProject = value => ({
    type: CreateProject.SET_ALL_LIST_PROJECT,
    value
})

export const LoadProject = () => async (dispatch, getState) => {
  
    axios.post('http://localhost:3030/allproject',{
    }).then((Response) =>{
        dispatch(setListProject(Response.data.data));
    });
   
}
export const setListMember = value => ({
    type: CreateMember.SET_ALL_LIST_MEMBER,
    value
})

export const LoadMember = () => async (dispatch, getState) => {
      axios.post('http://localhost:3030/allmember',{
      }).then((Response) =>{
          dispatch(setListMember(Response.data.data));
      });
     
  }
  

///-xu ly input----------
export const handleSelectDropProject = value => ({
    type: inputsTypes.SET_SELECT_DROP_PROJECT,
    value
})
export const ResetInput = () => ({
    type: inputsTypes.RESET_INPUTS,
})

export const setListMemberToProject = (value) => ({
    type: CreateProject.SET_LIST_MEMBER,
    value
})


export const handleChangeCheckBox = (event, value) => async (dispatch, getState) => {
    let listMember = getState().HomeReducer.formProject.listMember;

    if(listMember.indexOf(value) !== -1)
    {
        listMember.splice(listMember.indexOf(value), 1);
    }else{
        listMember.push(value);
    }
    dispatch(setListMemberToProject(listMember));

}


///end input------------

//edit form---

export const closeFormEditProject = () => (dispatch) => {
    dispatch(setIsShowFormEditProject(false));
    dispatch(resetFormEditProject());
    dispatch(resetListMemberInEditProject());
}

export const resetFormEditProject = () => ({
    type: EditProject.RESET_FORM_EDIT_PROJECT
})
export const resetListMemberInEditProject = () => ({
    type: EditProject.RESET_LIST_MEMBER_IN_EDIT_PROJECT
})


export const setIsShowFormEditProject = value => ({
    type: EditProject.SET_ISSHOW_FORM_Edit_PROJECT,
    value
})
export const setNameEditProject = value => ({
    type: EditProject.SET_NAME_EDIT_PROJECT,
    value
})
export const showFormEditProject = (value) => async (dispatch, getState) => {

    dispatch(setIsShowFormEditProject(true));
    dispatch(setNameEditProject(value));
    let Response = await axios.get('http://localhost:3030/getProject/' + value);
    dispatch(setListMemberToEditProject(Response.data.data));

}


export const setListMemberToEditProject = (value) => ({
    type: EditProject.SET_LIST_MEMBER_TO_EDIT_PROJECT,
    value
})

export const handleChangeCheckBoxEdit = (event, value) => async (dispatch, getState) => {
    let listMember = getState().HomeReducer.editProject.listMember;
    if(listMember.indexOf(value) !== -1)
    {
        listMember.splice(listMember.indexOf(value), 1);
    }else{
        listMember.push(value);
    }
    dispatch(setListMemberToEditProject(listMember));

}
export const submitFormEditProject = () => async (dispatch, getState) => {
    let name = getState().HomeReducer.editProject.nameProject;
    let listMember = getState().HomeReducer.editProject.listMember;
    await axios.post('http://localhost:3030/editproject',{
        name: name,
        listMember: listMember
    }).then((Response) =>{
        dispatch(closeFormEditProject());
        dispatch(LoadProject());
    });
   
}


//end edit form---


//----paging-----
export const setListProjectDetail = value => ({
    type: detailTypes.SET_LIST_PROJECT_DETAIL,
    value
})

export const setTotalPage = value => ({
    type: detailTypes.SET_TOTAL_PAGE,
    value
})
export const setPageCurrent = value => ({
    type: detailTypes.SET_PAGE_CURRENT,
    value
})

export const LoadDetailProject = (pageCur) => async (dispatch, getState) => {
    
      axios.post('http://localhost:3030/detailproject',{
        amount: 5,
        page: pageCur
      }).then((Response) =>{
          dispatch(setListProjectDetail(Response.data.data));
          dispatch(setTotalPage(Response.data.totalPage));
      });
     
}
export const handleKeyPress = (event)=>  async(dispatch, getState) => {
    let pageCur;
    if(event === 0)
    {
        pageCur = getState().HomeReducer.detailProject.pageCur;
    }
    else{
        pageCur = Number(event.value);
    }
    
    if(pageCur <= Number(getState().HomeReducer.detailProject.totalPage))
    {
        dispatch(setPageCurrent(pageCur));
    }
    dispatch(LoadDetailProject(getState().HomeReducer.detailProject.pageCur));
}


export const onclickPrev = value => (dispatch) => {
    dispatch(setPageCurrent(value - 1));  
    dispatch(handleKeyPress(0));
}
export const onclickNext = value => (dispatch) => {
    dispatch(setPageCurrent(value + 1));    
    dispatch(handleKeyPress(0));
}

//----end - paging-----