import { CreateMember, CreateProject, inputsTypes,EditProject, detailTypes} from '../constants/ActionTypes'
import { combineReducers } from 'redux'



const initStateMember =  {
    name:'',
    numberPhone:0,
    isShow: false,
    project: [],
    isValidNameMember: true
}

const initStateProject =  {
    nameProject:'',
    isShowProject: false,
    listMember:[],
    isValidNameProject: true
}

const initStateAll =  {
    listAllProject: [],
    listAllMember:[]
}

const initInput =  {
    isSelectDropProject: 0,
}

const detailProjectState =  {
    pageCur: 1,
    totalPage: 1,
    listProject: [],
}




const formMember = (state = initStateMember, action) => {
    switch(action.type){
        case CreateMember.SET_NAME:
            return Object.assign({}, state, {
                name: action.value
            });
        case CreateMember.SET_IS_VALID_NAME_MEMBER:
            return Object.assign({}, state, {
                isValidNameMember: action.value
            });
        case CreateMember.SET_NUMBER_PHONE:
            return Object.assign({}, state, {
                numberPhone: action.value
            });
        case CreateMember.SET_ISSHOW:
            return Object.assign({}, state, {
                isShow: true
            });
        case CreateMember.SET_ISSHOW_FORM:
            return Object.assign({}, state, {
                isShow: action.value
            });
        case CreateMember.RESET_FORM:
            return Object.assign({}, initStateMember);
        default:
            return state;
    }
}


const formProject = (state = initStateProject, action) => {
    switch(action.type){
        case CreateProject.SET_NAME_PROJECT:
            return Object.assign({}, state, {
                nameProject: action.value
            });
        case CreateProject.SET_IS_VALID_NAME_PROJECT:
            return Object.assign({}, state, {
                isValidNameProject: action.value
            });
        case CreateProject.SET_ISSHOW_PROJECT:
            return Object.assign({}, state, {
                isShowProject: true
            });
        case CreateProject.SET_ISSHOW_FORM_PROJECT:
            return Object.assign({}, state, {
                isShowProject: action.value
            });
        case CreateProject.RESET_FORM_PROJECT:
            return Object.assign({}, initStateProject);
        case CreateProject.SET_LIST_MEMBER:
            return Object.assign({}, state, {
                listMember: action.value
            });
        case CreateProject.RESET_LIST_MEMBER_IN_PROJECT:
            return Object.assign({}, state, {
                listMember: []
            });    
        default:
            return state;
    }
}
const allState = (state = initStateAll, action) => {
    switch(action.type){
        case CreateProject.SET_ALL_LIST_PROJECT:
            return Object.assign({}, state, {
                listAllProject: action.value
            });
        case CreateMember.SET_ALL_LIST_MEMBER:
            return Object.assign({}, state, {
                listAllMember: action.value
            });

        default:
            return state;
    }
}

const inputs = (state = initInput, action) => {
    switch(action.type){
        case inputsTypes.SET_SELECT_DROP_PROJECT:
            return Object.assign({}, state, {
                isSelectDropProject: action.value
            });
        case inputsTypes.RESET_INPUTS:
            return Object.assign({}, initInput);
        default:
            return state;
    }
}


const detailProject = (state = detailProjectState, action) => {
    switch(action.type){
        case detailTypes.SET_LIST_PROJECT_DETAIL:
            return Object.assign({}, state, {
                listProject: action.value
            });
        case detailTypes.SET_TOTAL_PAGE:
            return Object.assign({}, state, {
                totalPage: action.value
            });
        case detailTypes.SET_PAGE_CURRENT:
            return Object.assign({}, state, {
                pageCur: action.value
            });
        default:
            return state;
    }
}

const editProject = (state = initStateProject, action) => {
    switch(action.type){
        case EditProject.SET_ISSHOW_FORM_Edit_PROJECT:
            return Object.assign({}, state, {
                isShowProject: action.value
            });
        case EditProject.RESET_LIST_MEMBER_IN_EDIT_PROJECT:
            return Object.assign({}, state, {
                listMember: []
            });
        case EditProject.SET_LIST_MEMBER_TO_EDIT_PROJECT:
            return Object.assign({}, state, {
                listMember: action.value
            });
        case EditProject.RESET_FORM_EDIT_PROJECT:
            return Object.assign({}, initStateProject);
        case EditProject.SET_NAME_EDIT_PROJECT:
            return Object.assign({}, state, {
                nameProject: action.value
            });
        default:
            return state;
    }
}

export default combineReducers({
    formMember,
    formProject,
    allState,
    inputs,
    detailProject,
    editProject
})
