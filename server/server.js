var express =  require("express");
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors({ credentials: true, origin: 'http://localhost:3000' }));

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/projectmanagement');

app.get('/', function(res, req){
	req.send("This is Server");

});


var routes = require('./routes/index');

routes(app);

module.exports = app;
var port=Number(process.env.PORT || 3030);
app.listen(port, () => console.log("Server running on port " + port));