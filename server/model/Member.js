'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Member = new Schema({
    name: { type:String,required: true, unique: true},   
    numberPhone: {type:Number},
    project: {type: String}
});


module.exports = mongoose.model('Members', Member);