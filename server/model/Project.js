'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Project = new Schema({
    name: { type:String,required: true, unique: true},   
    listMember: {type: Array}
});


module.exports = mongoose.model('Projects', Project);