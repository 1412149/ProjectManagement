

var projectController = require('../controller/projectController');
var Members = require('../model/Member.js');
exports.createMember = function (req, res, next) {

	if(req.body !== null){
		Members.find({'name': req.body.name}, (err, member) =>{
			if(err)
			{
				res.send({status: 'false', message:"0"});
			}
			else{
				if(member.length === 0)
				{
					
                    var new_member = new Members({
                        name: req.body.name,
						numberPhone: Number(req.body.numberPhone),
						project: req.body.project
                    });
                    new_member.save((err, data)=>{
                        if(err){
                            res.send({status: 'false', message:"1"});
                        }
                        else{
							if(req.body.project !== "0"){
								if(projectController.UpdateMemberForProject(req.body.project, req.body.name) === true)
								{
									res.send({status: 'true', message:"2"});
								}
								else{
									res.send({status: 'true', message:"error"});
								}
							}
							
                        }
                    });
				
				}else{
					res.send({status: 'false', message:"4"});
				}
			}

			
		});
	}
};


exports.getAllMember = function (req, res, next) {
	Members.find({}, (err, data) =>{
		if(err)
		{
			res.send(err);
		}
		else{
			if(data != null)
			{
				res.send({status: 'true', message:"successfull", data: data});
			}else{
				res.send({status: 'false', message:"Not Exist"});
			}
		}
		
	});
};