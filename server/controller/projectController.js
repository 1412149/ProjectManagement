


var Projects = require('../model/Project.js');
exports.createProject = function (req, res, next) {
	if(req.body !== null){
		Projects.find({'name': req.body.name}, (err, project) =>{
			if(err)
			{
				res.send({status: 'false', message:"0"});
			}
			else{
				if(project.length === 0)
				{
                    var new_project = new Projects({
						name: req.body.name,
						listMember: req.body.listMember
                    });
                    new_project.save((err, data)=>{
                        if(err){
                            res.send({status: 'false', message:"1"});
                        }
                        else{
                            res.send({status: 'true', message:"2"});
                        }
                    });
				
				}else{
					res.send({status: 'false', message:"4"});
				}
			}

			
		});
	}
};


exports.getAllProject = function (req, res, next) {
	Projects.find({}, (err, data) =>{
		if(err)
		{
			res.send(err);
		}
		else{
			if(data != null)
			{
				res.send({status: 'true', message:"successfull", data: data});
			}else{
				res.send({status: 'false', message:"Not Exist"});
			}
		}
		
	});
};



exports.UpdateMemberForProject = function (nameProject, nameMember, next) {
	Projects.findOne({name: nameProject}, (err, data) =>{
		if(err)
		{
			return false;
		}
		else{
			if(data != null)
			{
				let temp = data.listMember;
				temp.push(nameMember);

				Projects.where({name: nameProject}).update({
					$set: {
						listMember: temp
					}
				}).update(function(err, result) {
						if (err) {
							return false;
						} else {
							return true;
						}
				});
			}else{
				return false;
			}
		}
		
	});

    
};



exports.getProject = function (req, res, next) {
	Projects.findOne({'name': req.params.name}, (err, data) =>{
		if(err)
		{
			res.send(err);
		}
		else{
			if(data != null)
			{
				res.send({status: 'true', message:"successfull", data: data.listMember});
			}else{
				res.send({status: 'false', message:"Not Exist"});
			}
		}
		
	});
};



exports.editproject = function (req, res, next) {
	if(req.body !== null){
		Projects.findOne({'name': req.body.name}, (err, project) =>{
			if(err)
			{
				res.send({status: 'false', message:"0"});
			}
			else{
				if(project !== null)
				{
	
					Projects.where({name: req.body.name}).update({
						$set: {
							listMember: req.body.listMember
						}
					}).update(function(err, result) {
							if (err) {
								res.send({status: 'false', message:"3"});
							} else {
								res.send({status: 'true', message:"success"});
							}
					});
				
				}else{
					res.send({status: 'false', message:"4"});
				}
			}

			
		});
	}
};



exports.detailproject = async (req, res, next) => {
	let mypage = parseInt(req.body.page);
	let myamount = parseInt(req.body.amount);
	let skip = (mypage-1) * myamount; 
	
	let allRecord =  await this.getTotalProject(); 
	let mtotalPage = parseInt((allRecord - 1)/myamount) + 1;
	Projects.find({})
	.limit(myamount)
	.skip(skip) 
	.exec(async function(err, data){
		if(err)
		{
			res.send(err);
		}
		else{
			if(data != null)
			{
				res.send({status: 'true', message:"successfull", data: data, totalPage: mtotalPage});
			}else{
				res.send({status: 'false', message:"Not Exist"});
			}
		}
		
	});
};

exports.getTotalProject = () => {
    return new Promise (resolve => {
        resolve(Projects.count({}));
    })
}