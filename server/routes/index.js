


module.exports = function(app){
    var memberController = require('../controller/memberController');
    var projectController = require('../controller/projectController');
    
    app.route('/createmember')
    .post(memberController.createMember);


    app.route('/allmember')
    .post(memberController.getAllMember);

    app.route('/createproject')
    .post(projectController.createProject);

    app.route('/allproject')
    .post(projectController.getAllProject);

    app.route('/getProject/:name')
    .get(projectController.getProject);

    app.route('/editproject')
    .post(projectController.editproject);

    app.route('/detailproject')
    .post(projectController.detailproject);
};
